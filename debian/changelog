ironic-ui (6.4.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090424).

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Dec 2024 08:15:14 +0100

ironic-ui (6.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Sep 2024 13:56:41 +0200

ironic-ui (6.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 29 Aug 2024 16:16:10 +0200

ironic-ui (6.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Apr 2024 21:07:30 +0200

ironic-ui (6.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Mar 2024 15:02:44 +0100

ironic-ui (6.2.1-3) unstable; urgency=medium

  * Remove useless depends on python3-six (Closes: #1053966).

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Jan 2024 10:33:25 +0100

ironic-ui (6.2.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 08 Oct 2023 18:43:47 +0200

ironic-ui (6.2.1-1) experimental; urgency=medium

  * New upstream release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Thu, 21 Sep 2023 16:46:04 +0200

ironic-ui (6.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-django-nose from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Sep 2023 13:59:29 +0200

ironic-ui (6.1.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1046169).

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Aug 2023 11:45:58 +0200

ironic-ui (6.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 16:23:21 +0200

ironic-ui (6.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Remove breaks, obsolete after Bookworm.
  * Removed (build-)depends versions satisfied after Bookworm.
  * Bumped debhelper-compat to 11.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Mar 2023 09:34:52 +0100

ironic-ui (6.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 18:21:01 +0200

ironic-ui (6.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed patches applied upstream:
    - django-4.x-ugettext_lazy-is-removed.patch
    - django-4.x-url-is-removed.patch

 -- Thomas Goirand <zigo@debian.org>  Mon, 12 Sep 2022 17:13:18 +0200

ironic-ui (5.1.0-3) unstable; urgency=medium

  * Fix Django 4.x compat (Closes: #1013515):
    - Add django-4.x-ugettext_lazy-is-removed.patch
    - Add django-4.x-url-is-removed.patch

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jun 2022 10:38:42 +0200

ironic-ui (5.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Mar 2022 13:51:35 +0200

ironic-ui (5.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Mar 2022 12:03:09 +0100

ironic-ui (5.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:21:15 +0200

ironic-ui (5.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:48:41 +0200

ironic-ui (5.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed testrepository from build-depends.
  * Require Horizon >= 3:20.0.0+git2020.09.21.27036cc0eb.

 -- Thomas Goirand <zigo@debian.org>  Tue, 14 Sep 2021 13:50:56 +0200

ironic-ui (4.3.0-4) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 16:14:00 +0200

ironic-ui (4.3.0-3) experimental; urgency=medium

  * Add rm_conffile to remove old files in /etc/openstack-dashboard/enable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 May 2021 11:40:59 +0200

ironic-ui (4.3.0-2) experimental; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.
  * Add Breaks: python3-django-horizon (<< 3:19.2.0-2~).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 May 2021 16:27:52 +0200

ironic-ui (4.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 22 Mar 2021 08:42:41 +0100

ironic-ui (4.2.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 10:33:28 +0200

ironic-ui (4.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 05 Oct 2020 10:03:55 +0200

ironic-ui (4.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 20:32:26 +0200

ironic-ui (4.0.0-3) unstable; urgency=medium

  * Move the package to the horizon-plugins Salsa group.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 12:15:18 +0200

ironic-ui (4.0.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Apr 2020 09:31:26 +0200

ironic-ui (3.5.3-1) unstable; urgency=medium

  * New upstream release.
  * Removed Breaks+Replaces: python-ironic-ui after Buster release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Oct 2019 09:03:27 +0200

ironic-ui (3.4.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Michal Arbet ]
  * Do not move files from source, copy it,
    kolla deployment expects that files are in /usr/lib...

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 22 Aug 2019 13:06:24 +0200

ironic-ui (3.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed package versions when satisfied in Buster.
  * Standards-Version: 4.3.0 (no change).
  * Only run unit tests, not integration.

 -- Thomas Goirand <zigo@debian.org>  Sun, 31 Mar 2019 21:42:38 +0200

ironic-ui (3.3.0-4) unstable; urgency=medium

  * Redesign ironic-ui:
      - Enabled files now in /etc/openstack-dashboard/
      - Removed post scripts which is now achieved by a trigger
  * d/copyright: Update copyright
  * d/control: Add me to uploaders field
  * d/control: Remove python2 support

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 21 Jan 2019 23:34:47 +0100

ironic-ui (3.3.0-3) unstable; urgency=medium

  * Reintroduce a python-ironic-ui to allow transition from Stretch because
    of the python-ironic-ui postrm needs to be neutralized. (Closes: #910225).

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Oct 2018 11:58:52 +0100

ironic-ui (3.3.0-2) unstable; urgency=medium

  * Fix postrm missing || (Closes: #900500).
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 20:46:54 +0200

ironic-ui (3.3.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Aug 2018 09:42:03 +0200

ironic-ui (3.1.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.
  * Switched to Python 3.

 -- Thomas Goirand <zigo@debian.org>  Wed, 21 Feb 2018 09:52:55 +0100

ironic-ui (3.0.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bumped debhelper compat version to 10

  [ Thomas Goirand ]
  * Standards-Version is 4.1.1 now (no change).
  * Deprecating priority extra as per policy 4.0.1.
  * Ran wrap-and-sort -bast.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Updating vcs fields.
  * Updating copyright format url.
  * Removed things defined in openstack-pkg-tools.

 -- Thomas Goirand <zigo@debian.org>  Fri, 03 Nov 2017 07:39:16 +0100

ironic-ui (2.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Correctly run collectstatic and compress after install.
  * Add ironic_ui/static/dashboard/admin/ironic/base-node/base-node.service.js
    to lintian-overrides.
  * Added missing copyright holder in debian/copyright.

 -- Thomas Goirand <zigo@debian.org>  Fri, 30 Sep 2016 11:09:28 +0200

ironic-ui (2.0.0-1) experimental; urgency=medium

  * Initial release. (Closes: #839076)

 -- Thomas Goirand <zigo@debian.org>  Wed, 28 Sep 2016 17:06:05 +0200
